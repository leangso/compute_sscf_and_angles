# python kaldi_sscf.py ./INPUT ./OUTPUT -saveAngleAndRaw 1 -halfRegWin 1 -averWin 2 -deltastep 2 -saveAngleReg 1

INPUT=/home/leangso/work/tmp/FAA
OUTPUT=/home/leangso/work/tmp/output3

averWin=3
deltastep=1

savePseudoEnergy=0
saveDeltaSSCF=1
saveRatios=0
saveAngleReg=0
saveSpeed=0
saveAngleAndRaw=1
savePolarSSCF=0
savePEH=0
saveSSCE=0
saveSubVar=0
saveSegments=0
saveSegSpeeds=0
saveSegAngles=0


python kaldi_sscf.py $INPUT $OUTPUT -averWin $averWin -deltastep $deltastep \
    -savePseudoEnergy $savePseudoEnergy \
    -saveDeltaSSCF $saveDeltaSSCF \
    -saveRatios $saveRatios \
    -saveAngleReg $saveAngleReg \
    -saveSpeed $saveSpeed \
    -saveAngleAndRaw $saveAngleAndRaw \
    -savePolarSSCF $savePolarSSCF \
    -savePEH $savePEH \
    -saveSSCE $saveSSCE \
    -saveSubVar $saveSubVar \
    -saveSegments $saveSegments \
    -saveSegSpeeds $saveSegSpeeds \
    -saveSegAngles $saveSegAngles