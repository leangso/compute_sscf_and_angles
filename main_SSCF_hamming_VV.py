# Author: Hang-Phuong Nguyen 2017
# Modified by Katia Vassor 2018

### import some essential sources for computing SSCF Angles

#.1 import source from  python_speech_features/python_speech_features folder, see base.py and sigproc.py files
##from python_speech_features import mfcc
##from python_speech_features import delta
from python_speech_features import ssc ## definition for computing Spectral Subband Centroid features from an audio signal, see base.py
from python_speech_features import aver3
from python_speech_features import sigproc ## definition for some algorithms of audio signal processing, see sigproc.py
from python_speech_features import save_results ## fonctions to save the results (text files and png figures)
from python_speech_features import countfile ##count the number of  wave files in a directoy
#from python_speech_features import graphical_interface as gi
from python_speech_features import callbacks as cb
import arctan_180


#.2 import some available sources from python library
import scipy.io.wavfile as wav
import math
import numpy
import os
import os.path
from scipy import signal
import matplotlib.pyplot as plt


#use main() if you want to analyse alle the wave files in a folder
#use main_one() if you want to analyse only one wave file

##path where you find Speec_Comm folder
path='C:/Users/tdebeure/Desktop/experimentationSSCF/Speech_Comm/'



def main(foldername, transition='VCV', nb_sscf=5, winlen=0.025, winstep=0.01):
    """
    Calcul the sscf and the angles and save  automatically the results (text files and figures)
    param foldername : name of the folder where the files.wav you want to analyse are"
    param transition : 'VV', 'VCV' or 'CVC_VC'. Default : 'VV'.
    param : nb_sscf is the number of SSCF you want to plot. Must be an integer between 1 and nfilt-1"
    param winlen : the length of the analysis window in seconds. Default is 0.025s (25 milliseconds)
    param winstep : the step between successive windows in seconds. Default is 0.01s (10 milliseconds)
    """

    ## Input path to VV database
    path_corpus=path+'Speech_Comm/'+transition+'/Corpus/'+foldername+'/'
    ##number of filters
    nfilt=6
    ## how many files in path
    Counter = countfile.counterfile(path_corpus)
    print('counter=',Counter)

    ## save filename in a list
    liste = os.listdir(os.path.expanduser(path_corpus))
    liste.sort()

    #treatment of each file
    for k in range(0,Counter):
        filename = liste[k]
        print(filename)
        os.chdir(path_corpus)

        (samplerate,signal)= wav.read(filename) ## return sample rate and signal

    ## compute SSCF features

        ssc_feat = ssc(signal,samplerate, nfilt=6)
        ssc_feat_int=ssc_feat.astype(int)
        #caster le tableau en int
        print(type(ssc_feat))
        print(type(ssc_feat[0,0]))
        print(ssc_feat[0,0])
        print(type(ssc_feat_int[0,0]))
        print(ssc_feat_int[0,0])
        frame_len = winlen*samplerate ## define frame
        frame_step = winstep*samplerate
        sig_len = len(signal) # return length of wav file
        frame_len = int(sigproc.round_half_up(frame_len))
        frame_step = int(sigproc.round_half_up(frame_step))
        if sig_len <= frame_len:
            numframes = 1
        else:
            numframes = 1 + int(math.ceil((1.0*sig_len - frame_len)/frame_step)) ## This method returns smallest integer not less than x.


    ## compute average SSCF, each three-SSCF parameter do an average

        aver3_sscf=aver3(ssc_feat, numframes)

    ## compute difference of SSCF average between current frame and the beginning frame

        delta_sscf = numpy.zeros(((numframes - 1),6))

        for i in range(0,(numframes - 1)):
            for j in range(0,6):
                #delta_sscf[i][j] = ssc_feat[i+1][j] - ssc_feat[i][j]
                delta_sscf[i][j] = aver3_sscf[i+1][j] - aver3_sscf[0][j]
                        #delta_sscf[i][j] = ssc_feat[i+1][j] - ssc_feat[0][j]


    ## compute SSCF Angles from SSCF difference

        angle = numpy.zeros(((numframes - 1),5))

        for i in range(0,(numframes - 1)):
            for j in range(0,5):
                angle[i][j] = arctan_180.arctan(delta_sscfi=delta_sscf[i][j], delta_sscfiplus1=delta_sscf[i][j+1])

    ## Extract angle results to file.txt and figures to file.png
        path_results=path+'Speech_Comm/'+transition+'/Results/'
        path_foldername=path_results+foldername+'/'
        save_results.save_txt(path_results, path_foldername, foldername, ssc_feat_int, angle, filename)
        #save_results.save_figure(foldername, path_results, path_foldername, signal,
                    #samplerate, filename, numframes, winstep, ssc_feat, angle, path, nb_sscf)

    return 1




#Analyse only one file and save the results
def main_one(audiofile, transition='VCV', nb_sscf=6, winlen=0.025, winstep=0.01):
    """
    Calcul the sscf and the angles and save  automatically the results (text files and figures)
    param audiofile : adress of the file.wav you want to analyse
    param transition : 'VV', 'VCV' or 'CVC_VC'. Default : 'VV'
    param : nb_sscf is the number of SSCF you want to plot. Must be an integer between 1 and nfilt-1"
    param winlen : the length of the analysis window in seconds. Default is 0.025s (25 milliseconds)
    param winstep : the step between successive windows in seconds. Default is 0.01s (10 milliseconds)
    """
    """main_one('E:/Mes documents/Speech_Comm/VCV/Corpus/idu/Idu_DNHien2.wav', 'VCV')"""

    audiofile.split('/')
    split=audiofile.split('/')
    filename=split[len(split)-1]
    path_corpus=''
    for i in range (0, len(split)-2) :
        path_corpus+=split[i]+'/'
    foldername=split[len(split)-2]+'/'
    path_foldername=path_corpus+foldername

    os.chdir(path_foldername)
    (samplerate,signal)= wav.read(filename) ## return sample rate and signal

    ## compute SSCF features

    ssc_feat = ssc(signal,samplerate, nfilt=6)
    frame_len = winlen*samplerate ## define frame
    frame_step = winstep*samplerate
    sig_len = len(signal) # return length of wav file
    frame_len = int(sigproc.round_half_up(frame_len))
    frame_step = int(sigproc.round_half_up(frame_step))
    if sig_len <= frame_len:
        numframes = 1
    else:
        numframes = 1 + int(math.ceil((1.0*sig_len - frame_len)/frame_step)) ## This method returns smallest integer not less than x.


    ## compute average SSCF, each three-SSCF parameter do an average

    aver3_sscf=aver3(ssc_feat, numframes)

    ## compute difference of SSCF average between current frame and the beginning frame

    delta_sscf = numpy.zeros(((numframes - 1),6))

    for i in range(0,(numframes - 1)):
        for j in range(0,6):
            delta_sscf[i][j] = aver3_sscf[i+1][j] - aver3_sscf[0][j]

    ## compute SSCF Angles from SSCF difference

    angle = numpy.zeros(((numframes - 1),5))

    for i in range(0,(numframes - 1)):
        for j in range(0,5):
            angle[i][j] = arctan_180.arctan(delta_sscfi=delta_sscf[i][j], delta_sscfiplus1=delta_sscf[i][j+1])

    #gi.build_interface(ssc_feat, angle)


    plt.plot(ssc_feat[:, 1], ssc_feat[:, 2], '+', linewith=1)
    plt.xlabel('SSCF1')
    plt.ylabel('SCCF2')
    plt.title('idu2 - SSCF1-SSCF2')

    plt.show()
    plt.clf()

    plt.plot(ssc_feat[:, 2], ssc_feat[:, 3], '+')
    plt.xlabel('SSCF2')
    plt.ylabel('SCCF3')
    plt.title('idu2 - SSCF2-SSCF3')

    plt.show()
    plt.clf()

    plt.plot(ssc_feat[:, 3], ssc_feat[:, 4], '+')
    plt.xlabel('SSCF3')
    plt.ylabel('SCCF4')
    plt.title('idu2 - SSCF3-SSCF4')

    plt.show()
    plt.clf()


    plt.plot(ssc_feat[:, 4], ssc_feat[:, 5], '+')
    plt.xlabel('SSCF4')
    plt.ylabel('SCCF5')
    plt.title('idu2 - SSCF4-SSCF5')

    plt.show()
    plt.clf()

    ## Extract angle results to file.txt and figures to file.png
    path_results=path+'Speech_Comm/'+transition+'/Results/'
    path_foldername=path_results+foldername+'/'
    save_results.save_txt(path_results, path_foldername, foldername, ssc_feat, angle, filename)
    save_results.save_figure(foldername, path_results, path_foldername, signal,
                samplerate, filename, numframes, winstep, ssc_feat, angle, path, nb_sscf)

    return 1



