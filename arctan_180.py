# -*- coding: utf-8 -*-
"""
Created on Thu Jul 06 15:26:10 2017

@author: Gary Gréteau 

Function Arctan -> [-180° , 180°] 

"""

import numpy

## define angle function
def arctan(delta_sscfi, delta_sscfiplus1):
    epsilon = 0.001
    if delta_sscfi <= epsilon and delta_sscfi >= -epsilon:
        return numpy.sign(delta_sscfi)*90
    if delta_sscfiplus1 > 0 and delta_sscfi > 0:
        return numpy.arctan(delta_sscfiplus1/delta_sscfi)*180/numpy.pi
    if delta_sscfiplus1 > 0 and delta_sscfi < 0:
        return (180 - numpy.arctan((-1)*delta_sscfiplus1/delta_sscfi)*180/numpy.pi)
    if delta_sscfiplus1 < 0 and delta_sscfi > 0:
        return numpy.arctan(delta_sscfiplus1/delta_sscfi)*180/numpy.pi
    if delta_sscfiplus1 < 0 and delta_sscfi < 0:
        return ((-1)*(180 - numpy.arctan(delta_sscfiplus1/delta_sscfi)*180/numpy.pi))
    #elif delta_sscfi < -epsilon:
    #   return -1*numpy.sign(numpy.arctan(delta_sscfiplus1/delta_sscfi)*180/numpy.pi )*180 + numpy.arctan(delta_sscfiplus1/delta_sscfi)*180/numpy.pi 
    #else:
    #   return numpy.arctan(delta_sscfiplus1/delta_sscfi)*180/numpy.pi
    
    
