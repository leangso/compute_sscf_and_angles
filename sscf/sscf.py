import numpy as np
import sscf.util as util


# 6 filterbanks from report of Thomas
def get_filterbanks(nfft=512):
    # generate frequency bins
    freq = np.array([0, 303, 738, 1768, 3055, 4220, 5842, 8000])    # from Thomas's report
    bin = np.floor((nfft + 1) * freq / 16000)

    # generate filterbanks
    fbank = np.zeros([6, nfft // 2 + 1])    # 6 x 257
    for j in range(0, 6):
        for i in range(int(bin[j]), int(bin[j + 1])):
            fbank[j, i] = (i - bin[j]) / (bin[j + 1] - bin[j])
        for i in range(int(bin[j + 1]), int(bin[j + 2])):
            fbank[j, i] = (bin[j + 2] - i) / (bin[j + 2] - bin[j + 1])
    return fbank


def compute_sscfs(frames, nfft=512):
    # compute power spectrum
    pspec = util.get_power_spectrum(frames, nfft)

    # compute filterbank energies
    fb = get_filterbanks(nfft)
    feat = np.dot(pspec, fb.T)  

    # compute centroids
    R = np.tile(np.linspace(1, 16000 / 2, np.size(pspec, 1)), (np.size(pspec, 0), 1))
    sscfs = np.dot(pspec * R, fb.T) / feat
    return sscfs


def compute_ratios(sscfs):
    num_frames, num_cols = sscfs.shape
    ratios = np.zeros((num_frames, num_cols - 1))
    for col in range(num_cols - 1):
        ratios[:, col] = sscfs[:, col] / sscfs[:, col + 1]
    return ratios


def compute_angles(sscfs, delta_step=1, delta_func=util.delta):
    # delta frames
    deltas = delta_func(sscfs, delta_step=delta_step)
    num_frames, num_cols = deltas.shape

    angles = np.zeros((num_frames, num_cols - 1))
    for col in range(num_cols - 1): 
        angles[:, col] = np.arctan2(deltas[:, col + 1], deltas[:, col])
    angles = angles * (180 / np.pi)
    return angles


def compute_distances(sscfs, delta_step=1, delta_func=util.delta):
    # delta frames
    deltas = delta_func(sscfs, delta_step=delta_step)
    num_frames, num_cols = deltas.shape

    distances = np.zeros((num_frames, num_cols - 1))
    for col in range(num_cols - 1):
        distances[:, col] = (deltas[:, col] ** 2 + deltas[:, col + 1] ** 2) ** 0.5
    return distances


def compute_speeds(sscfs, delta_step=1, delta_func=util.delta, win_len=0.025):
    distances = compute_distances(sscfs, delta_step=delta_step, delta_func=delta_func)
    num_samples = np.full((distances.shape[0], 1), delta_step * win_len * 16000)
    return distances / num_samples


def compute_ats(sscfs, delta_step=1, delta_func=util.delta, win_len=0.025):
    angles = compute_angles(sscfs, delta_step, delta_func)
    speeds = compute_speeds(sscfs, delta_step, delta_func, win_len)
    return angles * speeds
