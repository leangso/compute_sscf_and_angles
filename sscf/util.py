import numpy as np
import python_speech_features as psf

from python_speech_features import sigproc
from scipy.io import wavfile as wav


def read_wav(file):
    rate, signal = wav.read(file)
    return rate, signal


def get_frames(signal,
               sample_rate=16000,
               win_len=0.025,
               win_step=0.01,
               preemph=0.97,
               win_func=np.hamming):

    signal = sigproc.preemphasis(signal, preemph)
    frames = sigproc.framesig(sig=signal, frame_len=win_len * sample_rate, frame_step=win_step * sample_rate, winfunc=win_func)
    return frames


# input: NxD, output: Nx(NFFT/2+1)
def get_power_spectrum(frames, nfft=512):
    pspec = sigproc.powspec(frames, nfft)
    pspec = np.where(pspec == 0, np.finfo(float).eps, pspec)
    return pspec


def power2decibel(power_spectrum):
    return 10 * np.log10(power_spectrum)


def delta(frames, delta_step=1):
    padded = np.pad(frames, ((delta_step, 0), (0, 0)), mode='edge')
    delta = padded[delta_step:] - padded[:-delta_step]
    return delta


# delta proposed by Phuong
def delta_phuong(frames, win_len=0.025):
    padded = np.pad(frames, ((2, 2), (0, 0)), mode='edge')
    coeff = np.array([1, -0.8, 0, 0.8, -1]) # coefficient of each contexts

    delta = np.empty_like(frames)
    for t in range(len(frames)):
        delta[t] = np.dot(coeff, padded[t: t + 2 * 2 + 1]) / (win_len * 1000)
    return delta
    

def back_average(x, win_len=3, padding='edge'):
    padded = np.pad(x, (win_len - 1, 0), mode=padding)
    average = np.convolve(padded, np.ones(win_len) / win_len, mode='valid')
    return average


def average(x, win_len=3, padding='edge'):
    padded = np.pad(x, (0, win_len - 1), mode=padding)
    average = np.convolve(padded, np.ones(win_len) / win_len, mode='valid')
    return average


def back_averages(frames, win_len=3, padding='edge'):
    avg_frames = np.zeros_like(frames)
    for col in range(avg_frames.shape[1]):
        avg_frames[:, col] = back_average(frames[:, col], win_len, padding)
    return avg_frames


def averages(frames, win_len=3, padding='edge'):
    avg_frames = np.zeros_like(frames)
    for col in range(avg_frames.shape[1]):
        avg_frames[:, col] = average(frames[:, col], win_len, padding)
    return avg_frames
