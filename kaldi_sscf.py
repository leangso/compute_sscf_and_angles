# Author: Thomas Debeuret (inspired by main_SSCF_hamming_VV)
# This script compute the desired SSCF features and save them in kaldi ark format.

### import some essential sources for computing SSCF Angles

# .1 import source from  python_speech_features/python_speech_features folder, see base.py and sigproc.py files
##from python_speech_features import mfcc
##from python_speech_features import delta
from python_speech_features import ssc  ## definition for computing Spectral Subband Centroid features from an audio signal, see base.py
from python_speech_features import myAver
# from python_speech_features import aver3
from python_speech_features import sigproc  ## definition for some algorithms of audio signal processing, see sigproc.py
from python_speech_features import save_results  ## fonctions to save the results (text files and png figures)
from python_speech_features import countfile  ##count the number of  wave files in a directoy
# from python_speech_features import graphical_interface as gi
# from python_speech_features import callbacks as cb
# import arctan_180

# >>> Sotheara Edit
from sscf import sscf
from sscf import util
# <<< Sotheara Edit

# .2 import some available sources from python library
import sys
import scipy.io.wavfile as wav
import math
import numpy
import os
import os.path
from scipy import signal
import matplotlib.pyplot as plt
import argparse
from scipy.stats import linregress

# from sklearn.linear_model import LinearRegression

# Get arguments :
parser = argparse.ArgumentParser()
parser.add_argument("wavFolder", help="Input folder path that contains your wav file.")
parser.add_argument("outFolder", help="Folder path used to save your features in Kaldi format. Raw sscf are savec in outFolder/SSCF/, angles are save in outFolder/Angles/, pseudoEnergy is in ... and speed is in ...")
# parser.add_argument("-nfilt",type=int, help="Number of filters to apply. Default is 6.")
parser.add_argument("-winlen", type=float, help="Length of the analysis window in seconds. Default is 0.025 (25 milliseconds).")
parser.add_argument("-winstep", type=float, help="Step size between successive windows in seconds. Default is 0.01 (10 milliseconds).")
parser.add_argument("-deltastep", type=int, help="Step size used to compute angle and speed in number of window steps. Default is 10 window steps")
parser.add_argument("-halfRegWin", type=int, help="the linear regression will use (2*halfRegWin + 1) frames centred on the current point")
parser.add_argument("-linRegAbsDir", type=int, help="If 0 (default value), the angles from linear regression are corrected to have a range between -180 and 180. If 1, their range will be between -90 and 90.")
parser.add_argument("-averWin", type=int, help="Number of frames used to average the SSCFs by default = 3")
parser.add_argument("-thresholdFreq", type=float, help="Minimum frequency in Hz considered to compute the pseudo energy in high frequencies. default is 2000")
parser.add_argument("-savePseudoEnergy", type=int, help="If 0 (default value), doesn't save the pseudo-energy, save it if =1")
parser.add_argument("-saveDeltaSSCF", type=int, help="If 0 (default value), doesn't save the delta-sscf, save it if =1")
parser.add_argument("-saveRatios", type=int, help="If 0 (default value), doesn't save the sscf ratios, save it if =1")
parser.add_argument("-saveAngleReg", type=int, help="If 0 (default value), doesn't save the sscf angle computed from linear regression, save it if =1")
parser.add_argument("-saveSpeed", type=int, help="If 0 (default value), doesn't save the sscf speeds, save it if =1")
parser.add_argument("-saveAngleAndRaw", type=int, help="If 0 (default value), doesn't save the angle and doesn't save the raw sscf, save them if =1")
parser.add_argument("-savePolarSSCF", type=int, help="If 0 (default value), doesn't save a sscf representation in polar coordinates, save them if =1")
parser.add_argument("-savePEH", type=int, help="If 0 (default value), doesn't save the pseudo energy in high frequencies, save it if =1")
parser.add_argument("-saveSSCE", type=int, help="If 0 (default value), doesn't save the amplitude of each ssc, save them if =1")
parser.add_argument("-saveSubVar", type=int, help="If 0 (default value), doesn't save the spectral variance in each subband, save them if =1")
parser.add_argument("-saveSegments", type=int, help="If 0 (default value), doesn't save the sscf segment sizes, save them if =1 ; This option need segmentAnglesWin to be defined")
parser.add_argument("-saveSegSpeeds", type=int, help="If 0 (default value), doesn't save the sscf segment speeds, save them if =1 ; This option need segmentAnglesWin to be defined")
parser.add_argument("-saveSegAngles", type=int, help="If 0 (default value), doesn't save the sscf segment angle, save them if =1 ; This option need segmentAnglesWin to be defined")
parser.add_argument("-segmentAnglesWin", type=str, help="Coma separated list of angles in degree between 0 and 90 used to compute SSCF segment sizes speeds and angles, no default value. AngleWindow = 2 * segmentAngle. WARNING : use only coma without spaces to separate your angles values, the values MUST be given in assending order !!!! ex: -segmentAnglesWin 5,10,15")
args = parser.parse_args()
path_corpus = os.path.abspath(args.wavFolder) + '/'  ## Input path to wav database
outputFolder = os.path.abspath(args.outFolder) + '/'  ## Output path for results (subfolder SSCF and Angles)
savePseudoEnergy = args.savePseudoEnergy or 0
saveDeltaSSCF = args.saveDeltaSSCF or 0
saveRatios = args.saveRatios or 0
saveAngleReg = args.saveAngleReg or 0
saveSpeed = args.saveSpeed or 0
saveAngleAndRaw = args.saveAngleAndRaw or 0
savePolarSSCF = args.savePolarSSCF or 0
savePEH = args.savePEH or 0
saveSSCE = args.saveSSCE or 0
saveSubVar = args.saveSubVar or 0
saveSegments = args.saveSegments or 0
saveSegSpeeds = args.saveSegSpeeds or 0
saveSegAngles = args.saveSegAngles or 0

if (saveSegments or saveSegSpeeds or saveSegAngles):
    try:
        segmentAnglesWin = [float(item) for item in args.segmentAnglesWin.split(',')]
    except ValueError:
        print(repr(saveSegments) + repr(saveSegSpeeds) + repr(saveSegAngles))
        print('segmentAnglesWin is not set correctly !')
        print('-segmentAnglesWin = ' + args.segmentAnglesWin)
        sys.exit()
# nfilt = args.nfilt or 6
nfilt = 6  # Better to not change it, or you'll need to modify some other function in python_speech_features
winlen = args.winlen or 0.025
winstep = args.winstep or 0.01
halfRegWin = args.halfRegWin or 4  # the linear regression will use (2*halfRegWin + 1) points centred on the current point
# Delta step is number of frame to use to compute the sscf delta, which is then used to compute angle and speed
delta_step = args.deltastep or 10
linRegAbsDir = args.linRegAbsDir or 0
averWin = args.averWin or 3
thresholdFreq = args.thresholdFreq or 2000
print("\nkaldi_sscf options:")
print("delta step : " + repr(delta_step))
print("Length of the analysis window : " + repr(winlen) + " sec")
print("Step size between successive windows : " + repr(winstep) + " sec")
if (saveSegments or saveSegSpeeds or saveSegAngles):
    print('Angle windows for segment computing : ' + repr(segmentAnglesWin))
print("halfRegWin : " + repr(halfRegWin))
print("linRegAbsDir : " + repr(linRegAbsDir))
print("averWin : " + repr(averWin))
print("thresholdFreq : " + repr(thresholdFreq))
print("savePseudoEnergy : " + repr(savePseudoEnergy))
print("saveDeltaSSCF : " + repr(saveDeltaSSCF))
print("saveRatios : " + repr(saveRatios))
print("saveAngleReg : " + repr(saveAngleReg))
print("saveSpeed : " + repr(saveSpeed))
print("saveAngleAndRaw : " + repr(saveAngleAndRaw))
print("savePolarSSCF : " + repr(savePolarSSCF))
print("savePEH : " + repr(savePEH))
print("saveSSCE : " + repr(saveSSCE))
print("saveSubVar : " + repr(saveSubVar))
print("saveSegemnts : " + repr(saveSegments))
print("saveSegSpeeds : " + repr(saveSegSpeeds))
print("saveSegAngles : " + repr(saveSegAngles))

## how many files in path
Counter = countfile.counterfile(path_corpus)
print('counter=', Counter)

## save filename in a list
liste = os.listdir(os.path.expanduser(path_corpus))
liste.sort()

# treatment of each file
for k in range(0, Counter):
    filename = liste[k]
    print(filename)
    os.chdir(path_corpus)

    (samplerate, signal) = wav.read(filename)  ## return sample rate and signal

    ## compute SSCF features
    #    print("compute SSCF featurs.")
    # ssc_feat = ssc(signal, samplerate, nfilt=6)

    # >>> Sotheara Edit
    frames = util.get_frames(signal, win_len=winlen, win_step=winstep)
    ssc_feat = sscf.compute_sscfs(frames)
    # <<< Sotheara Edit

    ssc_feat_int = ssc_feat.astype(int)
    # caster le tableau en int
    print(type(ssc_feat))
    print(type(ssc_feat[0, 0]))
    print(ssc_feat[0, 0])
    print(type(ssc_feat_int[0, 0]))
    print(ssc_feat_int[0, 0])
    frame_len = winlen * samplerate  ## define frame
    frame_step = winstep * samplerate
    sig_len = len(signal)  # return length of wav file
    frame_len = int(sigproc.round_half_up(frame_len))
    frame_step = int(sigproc.round_half_up(frame_step))
    if sig_len <= frame_len:
        numframes = 1
    else:
        numframes = 1 + int(math.ceil(
            (1.0 * sig_len - frame_len) / frame_step))  ## This method returns smallest integer not less than x.

    ## compute pseudo-Energy in high frequencies (sum of the fft termes for frequencies greater than thresholdFreq) - Thomas Edit
    ## and compute Energie + pseudo-variance in each subband (in the same loop for quicker processing)
    if (savePEH or saveSSCE or saveSubVar):
        PEhigh = numpy.zeros(numframes)
        SSCEnergies = numpy.zeros(((numframes), 6))
        subVariances = numpy.zeros(((numframes), 6))
        myFilters = numpy.array([[0, 738.1], [303.3, 1767.8], [738.1, 3055.9], [1767.8, 4220.1], [3055.9, 5842.5],
                                 [4220.1, 8000]])  # subband limit frequencies of each used filters
        for i in range(0, (numframes)):
            winFFT = abs(numpy.fft.rfft(signal[(i * frame_step):(i * frame_step + frame_len)]))
            # print("samplerate : "+repr(samplerate))
            # print("winFFT.size : "+repr(winFFT.size))
            frequencies = numpy.fft.fftfreq(winFFT.size, d=1. / samplerate)
            PEhigh[i] = numpy.dot(winFFT, (frequencies >= thresholdFreq))
            if (saveSSCE or saveSubVar):
                for j in range(0, 6):
                    freqIndexes = (frequencies >= myFilters[j][0]) * (
                                frequencies <= myFilters[j][1])  # a*b is element wise multiplication,
                    SSCEnergies[i][j] = numpy.dot(winFFT, freqIndexes) / numpy.sum(freqIndexes)  # Amplitude of the SSCs
                    if (saveSubVar and (SSCEnergies[i][j] > 0)):
                        subVariances[i][j] = (1. / (myFilters[j][1] - myFilters[j][0])) * (
                                    1. / SSCEnergies[i][j]) * numpy.dot(freqIndexes * (winFFT), numpy.square(
                            frequencies - ssc_feat[i][j]))  # Variance in each subband

        if (savePEH):
            save_results.my_mkdir(outputFolder + "PEH")
            hfile = open(outputFolder + "PEH/" + os.path.basename(filename) + "_pseudo_energy_highFreq.txt", "w")
            numpy.savetxt(hfile, PEhigh, fmt='%1.3f')
            hfile.close()
        if (saveSSCE):
            save_results.my_mkdir(outputFolder + "SSCE")
            hfile = open(outputFolder + "SSCE/" + os.path.basename(filename) + "_SSCEnergies.txt", "w")
            numpy.savetxt(hfile, PEhigh, fmt='%1.3f')
            hfile.close()
        if (saveSubVar):
            save_results.my_mkdir(outputFolder + "subVar")
            hfile = open(outputFolder + "subVar/" + os.path.basename(filename) + "_subVariances.txt", "w")
            numpy.savetxt(hfile, subVariances, fmt='%1.3f')
            hfile.close()

    ## Compute pseudo-energy - Thomas Edit
    if (savePseudoEnergy == 1):
        #        print("compute pseudo energy.")
        penergy_array = numpy.zeros(((numframes - 1), 1))
        for i in range(0, (numframes - 1)):
            frame_penergy = 0.
            for j in range(0, frame_len):
                frame_penergy = frame_penergy + abs(signal[i * frame_step + j])
            penergy_array[i] = frame_penergy

        # save pseudo energy :
        save_results.my_mkdir(outputFolder + "PseudoEnergy")
        hfile = open(outputFolder + "PseudoEnergy/" + os.path.basename(filename) + ".txt", "w")
        numpy.savetxt(hfile, penergy_array, fmt='%1.3f')
        hfile.close()

    ## compute average SSCF, each three-SSCF parameter do an average - Thomas Edit
    #    print("compute SSCF average.")
    # aver_sscf = myAver(ssc_feat, numframes, averWin)
    # aver_sscf=aver3(ssc_feat, numframes)

    # >>> Sotheara Edit
    aver_sscf = util.averages(ssc_feat, win_len=averWin, padding='edge')
    # <<< Sotheara Edit

    ## compute polar coordinates in SSCF(i, i+1) plan :
    if (savePolarSSCF == 1):
        polarRaySSCF = numpy.zeros(((numframes), 5))
        polarAngleSSCF = numpy.zeros(((numframes), 5))
        for i in range(0, numframes):
            for j in range(0, 5):
                polarRaySSCF[i][j] = numpy.sqrt(numpy.square(aver_sscf[i][j]) + numpy.square(aver_sscf[i][j + 1]))
                polarAngleSSCF[i][j] = (180 / numpy.pi) * numpy.arctan2(aver_sscf[i][j + 1], aver_sscf[i][
                    j])  # as aver_sscf[i][j+1] and aver_sscf[i][j] are always positive
        save_results.my_mkdir(outputFolder + "polarSSCF")
        hfile = open(outputFolder + "polarSSCF/" + os.path.basename(filename) + "_ray.txt", "w")
        numpy.savetxt(hfile, polarRaySSCF, fmt='%1.3f')
        hfile.close()
        hfile = open(outputFolder + "polarSSCF/" + os.path.basename(filename) + "_angle.txt", "w")
        numpy.savetxt(hfile, polarAngleSSCF, fmt='%1.3f')
        hfile.close()

    ## compute difference of SSCF average between current frame and the beginning frame - Thomas Edit

    #    print("compute delta SSCF.")
    # delta_sscf = numpy.zeros(((numframes), 6))

    # for i in range(0, delta_step):
    #     for j in range(0, 6):
    #         delta_sscf[i][j] = aver_sscf[i][j] - aver_sscf[0][j]

    # for i in range((delta_step), (numframes - delta_step)):
    #     for j in range(0, 6):
    #         # delta_sscf[i][j] = ssc_feat[i+1][j] - ssc_feat[i][j]
    #         delta_sscf[i][j] = aver_sscf[i][j] - aver_sscf[i - delta_step][j]
    #         # delta_sscf[i][j] = ssc_feat[i+1][j] - ssc_feat[0][j]

    # for i in range((numframes - delta_step), numframes - 1):
    #     for j in range(0, 6):
    #         delta_sscf[i][j] = aver_sscf[(numframes - 1)][j] - aver_sscf[i][j]

    # >>> Sotheara Edit
    delta_sscf = util.delta(aver_sscf, delta_step=delta_step)
    # <<< Sotheara Edit

    # save deltaSSCF :
    if (saveDeltaSSCF == 1):
        save_results.my_mkdir(outputFolder + "deltaSSCF")
        hfile = open(outputFolder + "deltaSSCF/" + os.path.basename(filename) + ".txt", "w")
        numpy.savetxt(hfile, delta_sscf, fmt='%1.3f')
        hfile.close()

    # print(delta_sscf)
    #    delta_sscf = numpy.zeros(((numframes - 1),6))
    #
    #    for i in range(0,(numframes - 1)):
    #        for j in range(0,6):
    #            #delta_sscf[i][j] = ssc_feat[i+1][j] - ssc_feat[i][j]
    #            delta_sscf[i][j] = aver_sscf[i+1][j] - aver_sscf[0][j]
    #                    #delta_sscf[i][j] = ssc_feat[i+1][j] - ssc_feat[0][j]

    ## compute SSCF Angles from SSCF difference - Thomas Edit
    if (saveAngleAndRaw):
        #        print("compute angle.")
        angle = numpy.zeros(((numframes - 1), 5))

        for i in range(0, (numframes - 1)):
            for j in range(0, 5):
                # angle[i][j] = (180 / numpy.pi) * numpy.arctan2(delta_sscf[i][j], delta_sscf[i][j + 1])  # Thomas Edit : use of arctan2 fct from numpy
                angle[i][j] = (180 / numpy.pi) * numpy.arctan2(delta_sscf[i][j+1], delta_sscf[i][j]) # Sotheara Edit - fix order of parameters

        if (saveRatios == 1):
            ## Compute SSCF ratios (SSCF(i)/SSCF(i+1))
            SSCFratios = numpy.zeros(((numframes), 5))
            for i in range(0, numframes):
                for j in range(0, 5):
                    if (aver_sscf[i][
                        j + 1] == 0):  # just a security, it shouldn't happen as the sccf cannot be 0Hz (or maybe the first one)
                        SSCFratios[i][j] = 0
                    else:
                        SSCFratios[i][j] = aver_sscf[i][j] / aver_sscf[i][j + 1]
            # save the computed ratios
            save_results.my_mkdir(outputFolder + "sscf_ratios")
            hfile = open(outputFolder + "sscf_ratios/" + os.path.basename(filename) + ".txt", "w")
            numpy.savetxt(hfile, SSCFratios, fmt='%1.3f')
            hfile.close()

    ## Compute SSCF Angles from linear regression using halfRegWin points before and after the current point - Thomas Edit
    if (saveAngleReg or saveSegments or saveSegSpeeds or saveSegAngles):
        #        print("compute angle linreg.")
        angleReg = numpy.zeros(((numframes), 5))

        for i in range(0, numframes):
            pointsBefore = halfRegWin  # We want the halfRegWin points before the current point
            pointsAfter = halfRegWin  # We want the halfRegWin points after the current point
            if (pointsBefore > i):
                pointsBefore = i  # if there not enought points before the current point we take the maximum number of previous points available
            if (pointsAfter > numframes - i):
                pointsAfter = numframes - i  # if there not enought points after the current point we take the maximum number of next points available
            tmpSwap = numpy.swapaxes(aver_sscf[(i - pointsBefore):(i + pointsAfter + 1)], 0,
                                     1)  # We swap the subarray that contain the desired frame to get sscf in line
            for j in range(0, 5):
                if (all(numpy.isclose(tmpSwap[j], tmpSwap[j][0]))):
                    if (tmpSwap[j + 1][0] > tmpSwap[j + 1][-1]):
                        angleReg[i][j] = -90
                    elif (tmpSwap[j + 1][0] < tmpSwap[j + 1][-1]):
                        angleReg[i][j] = 90
                    else:
                        angleReg[i][j] = 0
                else:
                    # print("x="+repr(tmpSwap[j])+" y="+repr(tmpSwap[j+1]))
                    regressionLin = numpy.polyfit(tmpSwap[j], tmpSwap[j + 1], 1)
                    #    x=numpy.vstack([tmpSwap[j], numpy.ones(len(tmpSwap[j]))]).T
                    #    regressionLin = numpy.linalg.lstsq(x,tmpSwap[j+1])[0]
                    angleReg[i][j] = (180 / numpy.pi) * numpy.arctan(regressionLin[
                                                                         0])  # we only want the slope and not the offset so we take [0] (as: y = x * regressionLin[0] + regressionLin[1])
                    # Correction to have the angles between -180 and 180
                    if (linRegAbsDir == 0):
                        if (tmpSwap[j][0] > tmpSwap[j][
                            -1]):  # direction issue when last point is before first point on the absciss axis
                            if (angleReg[i][j] < 0):
                                angleReg[i][j] = angleReg[i][j] + 180
                            else:
                                angleReg[i][j] = angleReg[i][j] - 180
        #            for j in range(0,5):
        #                #regModel = LinearRegression(fit_intercept=False)
        #                #regModel.fit(tmpSwap[j],tmpSwap[j+1])
        #                #coef = regModel.coef_
        #                #slope = coef[0]
        #                slope, offset, r_value, p_value, std_err = linregress(tmpSwap[j],tmpSwap[j+1])
        #                if(math.isnan(slope)) :
        #                    #print("x="+repr(tmpSwap[j])+" and y="+repr(tmpSwap[j+1]))
        #                    if(tmpSwap[j+1][0]>tmpSwap[j+1][-1]) :
        #                        angleReg[i][j] = -90
        #                    elif(tmpSwap[j+1][0]<tmpSwap[j+1][-1]) :
        #                        angleReg[i][j] = 90
        #                    else :
        #                        angleReg[i][j] = 0
        #                #Correction to have the angles between -180 and 180
        #                else :
        #                    angleReg[i][j] = (180/numpy.pi)*numpy.arctan(slope)
        #                    if(linRegAbsDir == 0) :
        #                        if(tmpSwap[j][0]>tmpSwap[j][-1]): #direction issue when last point is before first point on the absciss axis
        #                            if(angleReg[i][j]<0):
        #                                angleReg[i][j]=angleReg[i][j]+180
        #                            else:
        #                                angleReg[i][j]=angleReg[i][j]-180
        if (saveAngleReg):
            # save angles from linear regression :
            save_results.my_mkdir(outputFolder + "angle_lin")
            hfile = open(outputFolder + "angle_lin/" + os.path.basename(filename) + ".txt", "w")
            numpy.savetxt(hfile, angleReg, fmt='%1.3f')
            hfile.close()

    ## compute speed - Thomas Edit
    if (saveSpeed == 1):
        #        print("compute speed.")
        speed = numpy.zeros(((numframes - 1), 5))

        for i in range(0, (numframes - 1)):
            for j in range(0, 5):
                speed[i][j] = numpy.sqrt(numpy.square(delta_sscf[i][j]) + numpy.square(delta_sscf[i][j + 1]))

        # save speed :
        save_results.my_mkdir(outputFolder + "Speed")
        hfile = open(outputFolder + "Speed/" + os.path.basename(filename) + ".txt", "w")
        numpy.savetxt(hfile, speed, fmt='%1.3f')
        hfile.close()

    ## compute segments sizes speeds and angles - Thomas Edit
    if (saveSegments or saveSegSpeeds or saveSegAngles):
        segments = numpy.zeros(((numframes), 5 * len(segmentAnglesWin)))
        segmentsSpeed = numpy.zeros(((numframes), 5 * len(segmentAnglesWin)))
        segmentsAngle = numpy.zeros(((numframes), 5 * len(segmentAnglesWin)))
        for i in range(0, numframes):
            for j in range(0, 5):
                currentAngle = angleReg[i][j]
                nbefore = 0  # number of previous consecutive points respecting the segmentAngle criteria
                nafter = 0  # number of following consecutive points respecting the segmentAngle criteria
                for k in range(0,
                               len(segmentAnglesWin)):  # for all specified angle window (no need to reset nbefore and nafter as the angles windows are ordered)
                    segmentAngle = segmentAnglesWin[k]
                    if (i - nbefore > 0):  # If it's not the first point (else no previous points)
                        # phi is the unsigned angle difference between the current angle and angles of previous points
                        phi = abs(currentAngle - angleReg[i - 1 - nbefore][j]) % 360
                        if (phi > 180):
                            phi = 360 - phi
                        while (
                                phi < segmentAngle):  # we count the number of previous points as long as they respect the segment angle criteria and the first point is not reached
                            nbefore = nbefore + 1
                            if (i - nbefore > 0):
                                phi = abs(currentAngle - angleReg[i - 1 - nbefore][j]) % 360
                                if (phi > 180):
                                    phi = 360 - phi
                            else:
                                phi = 360  # if first point is reached, assign big value to phi to go out of the loop
                    if (i + nafter < (numframes - 1)):  # If it's not the last point (else no following points)
                        phi = abs(currentAngle - angleReg[i + 1 + nafter][j]) % 360
                        if (phi > 180):
                            phi = 360 - phi
                        while (
                                phi < segmentAngle):  # we count the number of next points as long as they respect the segment angle criteria and the last point is not reached
                            nafter = nafter + 1
                            if (i + nafter < (numframes - 1)):
                                phi = abs(currentAngle - angleReg[i + 1 + nafter][j]) % 360
                                if (phi > 180):
                                    phi = 360 - phi
                            else:
                                phi = 360  # if last point is reached, assign big value to phi to go out of the loop
                    segments[i][j + 5 * k] = numpy.sqrt(
                        numpy.square(aver_sscf[i - nbefore][j] - aver_sscf[i + nafter][j]) + numpy.square(
                            aver_sscf[i - nbefore][j + 1] - aver_sscf[i + nafter][
                                j + 1]))  # Compute the euclidean distance between the extrem point of the segment
                    if (saveSegSpeeds):
                        if ((nbefore + nafter) == 0):  # if segment is size 0, save the instant speed
                            segmentsSpeed[i][j + 5 * k] = numpy.sqrt(
                                numpy.square(delta_sscf[i][j]) + numpy.square(delta_sscf[i][j + 1]))
                        else:  # else save speed from start point to end point
                            segmentsSpeed[i][j + 5 * k] = segments[i][j + 5 * k] / (nbefore + nafter)
                    if (saveSegAngles):
                        if ((nbefore + nafter) == 0):  # if segment is size 0, save the instant angle
                            segmentsAngle[i][j + 5 * k] = currentAngle
                        else:  # else save angle from start point to end point
                            segmentsAngle[i][j + 5 * k] = (180 / numpy.pi) * numpy.arctan2(
                                aver_sscf[i + nafter][j + 1] - aver_sscf[i - nbefore][j + 1],
                                aver_sscf[i + nafter][j] - aver_sscf[i - nbefore][j])

        if (saveSegments):
            # save segments :
            save_results.my_mkdir(outputFolder + "SegmentSizes")
            hfile = open(outputFolder + "SegmentSizes/" + os.path.basename(filename) + ".txt", "w")
            numpy.savetxt(hfile, segments, fmt='%1.3f')
            hfile.close()
        if (saveSegSpeeds):
            save_results.my_mkdir(outputFolder + "SegmentSpeeds")
            hfile = open(outputFolder + "SegmentSpeeds/" + os.path.basename(filename) + ".txt", "w")
            numpy.savetxt(hfile, segmentsSpeed, fmt='%1.3f')
            hfile.close()
        if (saveSegAngles):
            save_results.my_mkdir(outputFolder + "SegmentAngles")
            hfile = open(outputFolder + "SegmentAngles/" + os.path.basename(filename) + ".txt", "w")
            numpy.savetxt(hfile, segmentsAngle, fmt='%1.3f')
            hfile.close()

    ## Extract angle results to file.txt and figures to file.png
    # The save_results.save_txt function ask for unused arguments, not my scripts not my job to correct that ;p
    if (saveAngleAndRaw):
        path_results = 'This argument seems useless'
        foldername = 'This argument seems useless too'
        save_results.save_txt(path_results, outputFolder, foldername, ssc_feat_int, angle,
                              filename)  # This function doesn't work well with relative path
        # save_results.save_figure(foldername, path_results, path_foldername, signal,
        # samplerate, filename, numframes, winstep, ssc_feat, angle, path, nb_sscf)

