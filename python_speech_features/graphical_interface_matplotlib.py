import numpy as np
import matplotlib.pyplot as plt
from matplotlib.widgets import *
import matplotlib

def build_interface_matplotlib (ssc_feat, angle, numframes, winstep, nb_sscf) :
    #frames=np.linspace(1, numframes, numframes)
    #frames_times=frames*winstep
    #vect_axis_sscf=[0, np.max(frames_times), 0, np.max(ssc_feat[:,:nb_sscf+1]+80)+10]
    ssc1 = ssc_feat[:,1]
    ssc2 = ssc_feat[:,2]
    ssc3 = ssc_feat[:,3]
    ssc4 = ssc_feat[:,4]
    ssc5 = ssc_feat[:,5]
    rmv_pts_start=0
    rmv_pts_end=0

    fig, ax = plt.subplots()
    l, = ax.plot(ssc1[rmv_pts_start:numframes-1-rmv_pts_end], ssc2[rmv_pts_start:numframes-1-rmv_pts_end], lw=2, color='blue', marker='+')
    lrmvs, = ax.plot(ssc1[0:rmv_pts_start], ssc2[0:rmv_pts_start], lw=2, color='red', marker='+')
    lrmve, = ax.plot(ssc1[:numframes-1-rmv_pts_end], ssc2[:numframes-1-rmv_pts_end], lw=2, color='red', marker='+')
    vect_axis_sscf_sscf=[0, 7000, 0, 7000]
    plt.subplots_adjust(left=0.3)
    plt.axis(vect_axis_sscf_sscf)
    
    axcolor = 'lightgoldenrodyellow'
    rax = plt.axes([0.05, 0.7, 0.15, 0.15], facecolor=axcolor)
    radio_x = RadioButtons(rax, ('SSCF1', 'SSCF2', 'SSCF3', 'SSCF4', 'SSCF5'), active=0)
    def radio_sscf_x (label) :
        radio_correspondance = {'SSCF1': ssc1, 'SSCF2' : ssc2, 'SSCF3' : ssc3, 'SSCF4' : ssc4, 'SSCF5' : ssc5}
        xdata=radio_correspondance[label]
        l.set_xdata(xdata)
        #plt.axis([np.min(xdata)-80, np.max(xdata)+80, vect_axis_sscf_sscf[2], vect_axis_sscf_sscf[3]])
        plt.draw()
    radio_x.on_clicked(radio_sscf_x)

    rax = plt.axes([0.05, 0.4, 0.15, 0.15], facecolor=axcolor)
    radio_y = RadioButtons(rax, ('SSCF1', 'SSCF2', 'SSCF3', 'SSCF4', 'SSCF5'), active=1)
    def radio_sscf_y (label) :
        radio_correspondance = {'SSCF1': ssc1, 'SSCF2' : ssc2, 'SSCF3' : ssc3, 'SSCF4' : ssc4, 'SSCF5' : ssc5}
        ydata=radio_correspondance[label]
        l.set_ydata(ydata)
        #plt.axis([vect_axis_sscf_sscf[0], vect_axis_sscf_sscf[1], np.min(ydata)-80, np.max(ydata)+80]) 
        plt.draw()
    radio_y.on_clicked(radio_sscf_y)

    axcolor = 'lightgoldenrodyellow'
    axstart = plt.axes([0.25, 0.1, 0.65, 0.03], facecolor=axcolor)
    axend = plt.axes([0.25, 0.15, 0.65, 0.03], facecolor=axcolor)

    slider_start = Slider(axstart, 'Remove Start Points', 0, numframes//2, valinit=0, valstep=1)
    slider_end = Slider(axend, 'Remove End Points', 0, numframes//2, valinit=0, valstep=1)
    def update(val):
        removed_start=slider_start.val
        removed_end=slider_end.val
        rmv_pts_start.set()
        rmv_pts_end.set()

    plt.show()
    




