# Author: Hang-Phuong Nguyen 2017
### import some essential sources for computing SSCF Angles

#.1 import source from  python_speech_features/python_speech_features folder, see base.py and sigproc.py files 
from python_speech_features import mfcc
from python_speech_features import delta
from python_speech_features import logfbank
from python_speech_features import ssc ## definition for computing Spectral Subband Centroid features from an audio signal, see base.py
from python_speech_features import sigproc ## definition for some algorithms of audio signal processing, see sigproc.py

#.2 import some available sources from python library  
from array import array 
import scipy.io.wavfile as wav
import math 
import decimal
import numpy
import glob 
import os 
import os.path
import gzip
import sys
import struct
import time
import scipy as sp 
from scipy import signal

### Do compute SSCF Angles and write to Kaldi .ark format 
## Write numpy matrix into Kaldi .ark file 
readers = {}
try:
    import gzip
    readers['.gz'] = gzip.GzipFile
except ImportError:
    pass
try:
    import bz2
    readers['.bz2'] = bz2.BZ2File
except ImportError:
    pass
def smart_open(filename, mode = 'rb', *args, **kwargs):
    '''
    Opens a file "smartly":
      * If the filename has a ".gz" or ".bz2" extension, compression is handled
        automatically;
      * If the file is to be read and does not exist, corresponding files with
        a ".gz" or ".bz2" extension will be attempted.
    (The Python packages "gzip" and "bz2" must be installed to deal with the
        corresponding extensions)
    '''
    if 'r' in mode and not os.path.exists(filename):
        for ext in readers:
            if os.path.exists(filename + ext):
                filename += ext
                break
    extension = os.path.splitext(filename)[1]
    return readers.get(extension, open)(filename, mode, *args, **kwargs)
class KaldiWriteOut(object):

    def __init__(self, ark_path):

        self.ark_path = ark_path
        self.ark_file_write = smart_open(ark_path,"ab")

    def write_kaldi_mat(self, utt_id, utt_mat):
        utt_mat = numpy.asarray(utt_mat, dtype=numpy.float32)
        rows, cols = utt_mat.shape
        self.ark_file_write.write(struct.pack('<%ds'%(len(utt_id)), utt_id))
        self.ark_file_write.write(struct.pack('<cxcccc', ' ','B','F','M',' '))
        self.ark_file_write.write(struct.pack('<bi', 4, rows))
        self.ark_file_write.write(struct.pack('<bi', 4, cols))
        self.ark_file_write.write(utt_mat)

    def close(self):
        self.ark_file_write.close()

## define window 
def round_half_up(number):
    return int(decimal.Decimal(number).quantize(decimal.Decimal('1'), rounding=decimal.ROUND_HALF_UP))

## define angle function
def arctan(delta_sscfi, delta_sscfiplus1):
	epsilon = 0.001
	if delta_sscfi <= epsilon and delta_sscfi >= -epsilon:
		return numpy.sign(delta_sscfi)*90
	if delta_sscfiplus1 > 0 and delta_sscfi > 0:
		return numpy.arctan(delta_sscfiplus1/delta_sscfi)*180/numpy.pi
	if delta_sscfiplus1 > 0 and delta_sscfi < 0:
		return (180 - numpy.arctan((-1)*delta_sscfiplus1/delta_sscfi)*180/numpy.pi)
	if delta_sscfiplus1 < 0 and delta_sscfi > 0:
		return numpy.arctan(delta_sscfiplus1/delta_sscfi)*180/numpy.pi
	if delta_sscfiplus1 < 0 and delta_sscfi < 0:
		return ((-1)*(180 - numpy.arctan(delta_sscfiplus1/delta_sscfi)*180/numpy.pi))
	#elif delta_sscfi < -epsilon:
	#	return -1*numpy.sign(numpy.arctan(delta_sscfiplus1/delta_sscfi)*180/numpy.pi )*180 + numpy.arctan(delta_sscfiplus1/delta_sscfi)*180/numpy.pi 
	#else:
	#	return numpy.arctan(delta_sscfiplus1/delta_sscfi)*180/numpy.pi
 

## continuous speech database for training and test, see in digits_audio folder  
datas = ['train', 'test']
for data in datas:
	
	## speakers in data for training 
	if (data == 'train'):
		speakers = ['M00','M01','M02','M03','M04','M05']
	## speakers in data for test
	if (data == 'test'):
		speakers = ['F05']
	for speaker in speakers:
		path='/home/mica/MyKaldi/kaldi-trunk/egs/demo_sscf/digits_audio/' + data + '/'+ speaker + '/wav'


 

## how many files in path 

		Counter = 0
		for root, dirs, files in os.walk(path):
			for file in files:    
				if file.endswith('.wav'):
					Counter += 1
		print(Counter)
## save filename in a list  
		list = os.listdir(os.path.expanduser(path))
		list.sort()
		print (len(list))
		for k in range(0,Counter):
			filename = list[k]
			os.chdir(path)
## compute SSCF features
			(samplerate,signal)= wav.read(filename) ## return sample rate and signal 
			ssc_feat = ssc(signal,samplerate,nfilt=6)	
			winlen=0.025 ## define windown 
			winstep=0.01
			frame_len = winlen*samplerate ## define frame 
			frame_step = winstep*samplerate
			slen = len(signal) # return length of wav file 
			frame_len = int(round_half_up(frame_len))
			frame_step = int(round_half_up(frame_step))
			if slen <= frame_len:
				numframes = 1
			else:
				numframes = 1 + int(math.ceil((1.0*slen - frame_len)/frame_step)) ## This method returns smallest integer not less than x.
			print(numframes) ## print number of frames 
			print(frame_len)
			print(frame_step)
			print(slen)
			print(samplerate)
	

## compute average SSCF, each three-SSCF parameter do an average 

			aver3_sscf = numpy.zeros((numframes,6))

			for i in range(0,numframes):
				for j in range(0,6):
					if ((numframes - i) > 2):
						tgsscf = [ssc_feat[i][j], ssc_feat[i+1][j], ssc_feat[i+2][j]]
						aver3_sscf[i][j] = sum(tgsscf)/len(tgsscf)
					if ((numframes - i) == 2):
						tgsscf = [ssc_feat[i][j], ssc_feat[i+1][j]]
						aver3_sscf[i][j] = sum(tgsscf)/len(tgsscf)
					if ((numframes - i) == 1):
						tgsscf = [ssc_feat[i][j]]
						aver3_sscf[i][j] = sum(tgsscf)/len(tgsscf)

## compute difference of SSCF average between current frame and the beginning frame 

			delta_sscf = numpy.zeros(((numframes - 1),6))

			for i in range(0,(numframes - 1)):
				for j in range(0,6):
			
					delta_sscf[i][j] = aver3_sscf[i+1][j] - aver3_sscf[0][j]
					
					
				


## compute SSCF Angles from SSCF difference  
			t = numpy.zeros(((numframes - 1),5))
			rad = numpy.zeros(((numframes - 1),5))
			angle = numpy.zeros(((numframes - 1),5))


			for i in range(0,(numframes - 1)):
				for j in range(0,5):
					angle[i][j] = arctan(delta_sscfi=delta_sscf[i][j], delta_sscfiplus1=delta_sscf[i][j+1])
				
	


## Extract SSCF angle results to file.ark 
			

			text, extension = os.path.splitext(list[k])
	    		print(text)
			utt_id = speaker + '-' + text 
			print(utt_id)
			ark_path_angles = '/home/mica/MyKaldi/kaldi-trunk/egs/demo_sscf/digits_audio/audio/sscf/feature_' + data + '_angles.ark' 
			output = KaldiWriteOut(ark_path_angles)
			output.write_kaldi_mat(utt_id,angle)
			




	
