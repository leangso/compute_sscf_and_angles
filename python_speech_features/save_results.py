import os
import matplotlib.pyplot as plt
import numpy
import errno

import math
import scipy.io.wavfile as wav

#create a directory 'name' and do nothing if such a directory already exists
def my_mkdir(name):
     try:
         os.mkdir(name)
     except OSError as e:
         if not (e.errno == errno.EEXIST and os.path.isdir(name)):
             raise

#save the sscf values and the angles values in text files
def  save_txt(path_results, path_foldername, foldername, ssc_feat_int, angle, filename) :
    text, extension = os.path.splitext(filename)
    print(text)
    utt_id = text 
    my_mkdir(path_foldername)
    
    #save SSCF
    path_to_save_sscf = path_foldername+'SSCF'
    os.chdir(path_foldername)
    my_mkdir(path_to_save_sscf)
    os.chdir(path_to_save_sscf)
    print('save results :', ssc_feat_int[0,0])
    numpy.savetxt(utt_id+'_sscf.txt',ssc_feat_int, fmt='%1.4d')##convertir en entier (cast)

    #save angles
    path_to_save_angles = path_foldername+'Angles'
    os.chdir(path_foldername)
    my_mkdir(path_to_save_angles)
    os.chdir(path_to_save_angles)
    numpy.savetxt(utt_id+'_angle.txt',numpy.floor(angle))


    
#save the figures plotted with matplotlib
def save_figure(foldername, path_results, path_foldername, signal, samplerate,
                filename, numframes, winstep, ssc_feat, angle, path, nb_sscf) :
    text, extension = os.path.splitext(filename)
    filename = text
    
    #axis definition  
    times=numpy.arange(len(signal))/samplerate        
    frames=numpy.linspace(1, numframes, numframes)
    frames_times=frames*winstep
    vect_axis_signal=[0, numpy.max(frames_times), numpy.min(signal-80), numpy.max(signal+80)]
    vect_axis_sscf=[0, numpy.max(frames_times), 0, numpy.max(ssc_feat[:,:nb_sscf+1]+80)+10]
    
    (i_s, i_t, i_a)=find_silence(numframes, signal)
    ind_del=int(math.ceil(i_a[1]/6))        #nb of points deleted at the begining and at the end of the signal.
                                            #These points are deleted because they are not part of the transition

    ##plot signal, sscf, angles on the same figure and save the figure
    path_to_save_figures = path_foldername+'Figures'
    os.chdir(path_foldername)
    my_mkdir(path_to_save_figures)          #create the directory where the figures will be saved
    
    #signal
    plt.subplot(411)                        #plot the audio signal with different colors crresponding to the transition
    plt.plot(times[:i_s[0]], signal[0:i_s[0]], 'c')
    plt.plot(times[i_s[0]: i_t[0]], signal[i_s[0]:i_t[0]], 'b')
    plt.plot(times[i_t[0]:i_s[1]], signal[i_t[0]:i_s[1]], 'r')
    plt.plot(times[i_s[1]: i_t[1]], signal[i_s[1]: i_t[1]], 'm')
    plt.plot(times[i_t[1]:i_s[2]], signal[i_t[1]:i_s[2]], 'g') 
    plt.plot(times[i_s[2]: i_t[2]], signal[i_s[2]: i_t[2]], 'y')
    if i_t[2]!=0:
             plt.plot(times[i_t[2]:], signal[i_t[2]:], 'k')
    plt.title(filename)
    plt.ylabel('Audiosignal',fontsize=8)
    plt.xlabel('Time',fontsize=8)
    plt.axis(vect_axis_signal)

    #sscf                                   #plot the number of SSCF indicated as a parameter of the main()
    plt.subplot(412)
    for k in range(1, nb_sscf+1):
        plt.plot(frames_times, ssc_feat[:,k], label="SSCF"+str(k))
    plt.ylabel('SSCF')
    plt.xlabel('Num Frame',fontsize=8)
    plt.axis(vect_axis_sscf)
    plt.legend(loc='lower left', bbox_to_anchor=(1, 0), prop={'size':8}, markerfirst=False)

    
    #angle sscf1-sscf2
    plt.subplot(413)
    plt.plot(angle[i_a[0]+ind_del:abs(i_a[1]-ind_del),1],angle[i_a[0]+ind_del:abs(i_a[1]-ind_del),2], 'b', linewidth=1, marker="+")
    plt.plot(angle[i_a[2]+ind_del:abs(i_a[3]-ind_del),1],angle[i_a[2]+ind_del:abs(i_a[3]-ind_del),2], 'm', linewidth=1, marker="+")
    plt.plot(angle[i_a[4]+ind_del:abs(i_a[5]-ind_del),1],angle[i_a[4]+ind_del:abs(i_a[5]-ind_del),2], 'y', linewidth=1, marker="+")
    plt.ylim(-180, 180)
    plt.ylabel('SSCF2',fontsize=8)
    plt.xlabel('SSCF1',fontsize=8)

    #angle sscf2-sscf3
    plt.subplot(414)
    plt.plot(angle[i_a[0]+ind_del:abs(i_a[1]-ind_del),2],angle[i_a[0]+ind_del:abs(i_a[1]-ind_del),3], 'b', linewidth=1, marker="+")
    plt.plot(angle[i_a[2]+ind_del:abs(i_a[3]-ind_del),2],angle[i_a[2]+ind_del:abs(i_a[3]-ind_del),3], 'm', linewidth=1, marker="+")
    plt.plot(angle[i_a[4]+ind_del:abs(i_a[5]-ind_del),2],angle[i_a[4]+ind_del:abs(i_a[5]-ind_del),3], 'y', linewidth=1, marker="+")
    plt.ylim(-180, 180)
    plt.ylabel('SSCF3',fontsize=8)
    plt.xlabel('SSCF2',fontsize=8)
    
    os.chdir(path_to_save_figures)
    plt.savefig(filename+'.png', dpi=150)
    
    #plot signal and sscf and save the figure
    #signal
    plt.subplot(211)
    plt.plot(times[:i_s[0]], signal[0:i_s[0]], 'c')
    plt.plot(times[i_s[0]: i_t[0]], signal[i_s[0]:i_t[0]], 'b')
    plt.plot(times[i_t[0]:i_s[1]], signal[i_t[0]:i_s[1]], 'r')
    plt.plot(times[i_s[1]: i_t[1]], signal[i_s[1]: i_t[1]], 'm')
    plt.plot(times[i_t[1]:i_s[2]], signal[i_t[1]:i_s[2]], 'g') 
    plt.plot(times[i_s[2]: i_t[2]], signal[i_s[2]: i_t[2]], 'y')
    if i_t[2]!=0:
             plt.plot(times[i_t[2]:], signal[i_t[2]:], 'k')
    plt.title(filename)
    plt.ylabel('Audiosignal')
    plt.xlabel('Time')
    plt.axis(vect_axis_signal, label='axe1')
    

    #sscf
    plt.subplot(212)
    for k in range(1, nb_sscf+1):
        plt.plot(frames_times, ssc_feat[:,k], label="SSCF"+str(k))
    plt.ylabel('SSCF')
    plt.xlabel('Num Frame')
    plt.axis(vect_axis_sscf, label='axe2')

    plt.legend(loc='lower left', bbox_to_anchor=(1, 0), prop={'size':8}, markerfirst=False)
    plt.savefig(filename+'_sscf.png', dpi=150)
    plt.clf()

    #plot angles (whithout the first ind_del indices and the last ind_del indices) and save the figure 
            
    plt.subplot(211)
    plt.title(filename)
    plt.plot(angle[i_a[0]+ind_del:abs(i_a[1]-ind_del),1],angle[i_a[0]+ind_del:abs(i_a[1]-ind_del),2], 'b', linewidth=1, marker="+")
    plt.plot(angle[i_a[2]+ind_del:abs(i_a[3]-ind_del),1],angle[i_a[2]+ind_del:abs(i_a[3]-ind_del),2], 'm', linewidth=1, marker="+")
    plt.plot(angle[i_a[4]+ind_del:abs(i_a[5]-ind_del),1],angle[i_a[4]+ind_del:abs(i_a[5]-ind_del),2], 'y', linewidth=1, marker="+")
    plt.ylim(-180, 180)
    plt.ylabel('SSCF2',fontsize=8)
    plt.xlabel('SSCF1',fontsize=8)

    
    plt.subplot(212)
    plt.plot(angle[i_a[0]+ind_del:abs(i_a[1]-ind_del),2],angle[i_a[0]+ind_del:abs(i_a[1]-ind_del),3], 'b', linewidth=1, marker="+")
    plt.plot(angle[i_a[2]+ind_del:abs(i_a[3]-ind_del),2],angle[i_a[2]+ind_del:abs(i_a[3]-ind_del),3], 'm', linewidth=1, marker="+")
    plt.plot(angle[i_a[4]+ind_del:abs(i_a[5]-ind_del),2],angle[i_a[4]+ind_del:abs(i_a[5]-ind_del),3], 'y', linewidth=1, marker="+")
    plt.ylabel('SSCF3',fontsize=8)
    plt.xlabel('SSCF2',fontsize=8)
    plt.ylim(-180, 180)
    plt.savefig(filename+'_angles.png', dpi=150)
    plt.clf()




#Find the silences in the signal, in order to plot the angles with different colours corresponding to the different transition
#Won't find more than 4 silences and 3 transtions
def find_silence(numframes, signal):
    tab_signal=abs(signal)
    threshold=190                       #threshold separating sound from silence
    time_threshold=5000                 #nb of points min under the threshold to be a silence
    (i_s0, i_s1, i_s2, i_s3)=(0,0,0,0)
    (i_t1, i_t2, i_t3)=(0,0,0)
    n=0
    i=0
    while (i<len(tab_signal) and tab_signal[i]<threshold):  #first silence
         n+=1
         i+=1
    if (n>=time_threshold and i<len(tab_signal)):
         i_s0=i
    n=0
    while (i_s1==0 and i<len(tab_signal)):                  #first transition
         while (i<len(tab_signal) and tab_signal[i]>=threshold):
              i+=1
         i_t1=i
         while (i<len(tab_signal) and tab_signal[i]<threshold):
              n+=1
              i+=1
         if n>=time_threshold:                              #second silence
              i_s1=i
         else :
              i_t1=i
    n=0
    while (i_s2==0 and i<len(tab_signal)):                  #second transition
         while (i<len(tab_signal) and tab_signal[i]>=threshold):
              i+=1
         i_t2=i
         while ((i<len(tab_signal) and tab_signal[i]<threshold)):
              n+=1
              i+=1
         if n>=time_threshold:
              i_s2=i
         else :
              i_t2=i
    n=0
    while (i_s3==0 and i<len(tab_signal)):                  #third transition
         while (i<len(tab_signal) and tab_signal[i]>=threshold):
              i+=1
         i_t3=i
         while (i<len(tab_signal) and tab_signal[i]<threshold):
              n+=1
              i+=1
         if n>=time_threshold:
              i_s3=i
         else :
              i_t3=i

    #conversion to indices in the angles array
    i_a11=math.ceil(numframes*i_s0/len(tab_signal))
    i_a12=math.ceil(numframes*i_t1/len(tab_signal))
    i_a21=math.ceil(numframes*i_s1/len(tab_signal))
    i_a22=math.ceil(numframes*i_t2/len(tab_signal))
    i_a31=math.ceil(numframes*i_s2/len(tab_signal))
    i_a32=math.ceil(numframes*i_t3/len(tab_signal))

    i_s=(i_s0, i_s1, i_s2, i_s3)
    i_t=(i_t1, i_t2, i_t3)
    i_a=(i_a11, i_a12, i_a21, i_a22, i_a31, i_a32)
    return (i_s, i_t, i_a)



    

