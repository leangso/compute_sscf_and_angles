#!/usr/bin/env python

from python_speech_features import logfbank
from python_speech_features import ssc
from python_speech_features import sigproc
from array import array 
import scipy.io.wavfile as wav
import math 
import decimal
import numpy
import glob 
import os

#Counts the wave files in a directory
def counterfile(path):
    Counter = 0
    for root, dirs, files in os.walk(path):
        for file in files:    
            if file.endswith('.wav'):
               Counter += 1
    return Counter

