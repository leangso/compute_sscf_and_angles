#Build the graphical interface for main_one()
from tkinter import *
import tkinter as tk
from python_speech_features import callbacks as cb
import matplotlib
matplotlib.use("TkAgg")
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, NavigationToolbar2TkAgg
from matplotlib.figure import Figure

def create_menu(window):
    menubar = Menu(window)
    menu1 = Menu(menubar, tearoff=0)
    menu1.add_command(label="Create", command=cb.test)
    menu1.add_command(label="Edit", command=cb.test)
    menu1.add_separator()
    menu1.add_command(label="Quit", command=window.quit)
    menubar.add_cascade(label="Files", menu=menu1)

    menu2 = Menu(menubar, tearoff=0)
    menu2.add_command(label="Cut", command=cb.test)
    menu2.add_command(label="Copy", command=cb.test)
    menu2.add_command(label="Paste", command=cb.test)
    menubar.add_cascade(label="Edit", menu=menu2)

    menu3 = Menu(menubar, tearoff=0)
    menu3.add_command(label="About...", command=cb.test)
    menubar.add_cascade(label="Help", menu=menu3)

    menu4 = Menu(menubar, tearoff=0)
    menu4.add_command(label="SSCF1-SSCF2", command=lambda : cb.trace_SSCF1_SSCF2(ssc_feat))
    menu4.add_command(label="SSCF2-SSCF3", command=lambda : cb.trace_SSCF2_SSCF3(ssc_feat))
    menu4.add_command(label="SSCF3-SSCF4", command=lambda : cb.trace_SSCF3_SSCF4(ssc_feat))
    menu4.add_command(label="SSCF4-SSCF5", command=lambda : cb.trace_SSCF4_SSCF5(ssc_feat))
    menubar.add_cascade(label="Trace Trajectories", menu=menu4)

    menu5 = Menu(menubar, tearoff=0)
    menu5.add_command(label="Angle SSCF1-SSCF2", command=lambda : cb.trace_SSCF1_SSCF2(ssc_feat))
    menu5.add_command(label="Angle SSCF2-SSCF3", command=lambda : cb.trace_SSCF2_SSCF3(ssc_feat))
    menu5.add_command(label="Angle SSCF3-SSCF4", command=lambda : cb.trace_SSCF3_SSCF4(ssc_feat))
    menu5.add_command(label="Angle SSCF4-SSCF5", command=lambda : cb.trace_SSCF4_SSCF5(ssc_feat))
    menubar.add_cascade(label="Trace Angles", menu=menu4)
    window.config(menu=menubar)
    

def build_interface(ssc_feat, angles):
##    class GraphPage(tk.Frame):
##
##        def __init__(self, parent):
##            tk.Frame.__init__(self, parent)
##            self.title_label = tk.Label(self, text="Graph Page Example")
##            self.title_label.pack()
##            self.pack()
##
##        def add_mpl_figure(self, fig):
##            self.mpl_canvas = FigureCanvasTkAgg(fig, self)
##            self.mpl_canvas.show()
##            self.mpl_canvas.get_tk_widget().pack(side=tk.TOP, fill=tk.BOTH, expand=True)
##
##            self.toolbar = NavigationToolbar2TkAgg(self.mpl_canvas, self)
##            self.toolbar.update()
##            self.mpl_canvas._tkcanvas.pack(side=tk.TOP, fill=tk.BOTH, expand=True)
##
##
##    class MPLGraph(Figure):
##
##        def __init__(self,xdata, ydata):
##            Figure.__init__(self, figsize=(5, 5), dpi=100)
##            self.plot = self.add_subplot(111)
##            self.plot.plot(xdata, ydata, marker='+')
    #creation of the window
    window1 = Tk(screenName='Interface')
    window1.geometry('500x500')
    #label = Label(window1, text="Hello World")
    #label.pack()
    #creation of the buttons
    bt_SSCF=Button(window1, text="SSCF trajectories", command=lambda: cb.create_graph_sscf(ssc_feat, window1))
    bt_SSCF.pack()
    #creation of the menu
    create_menu(window1)
    #creation of the pane
    pane_win= PanedWindow(window1, orient=HORIZONTAL)
    pane_win.pack(side=TOP, expand=Y, fill=BOTH, pady=2, padx=2)
    pane_win.add(Label(pane_win, text='Volet 1', background='white', anchor=CENTER))
    pane_win.add(Label(pane_win, text='Volet 2', background='white', anchor=CENTER))
    pane_win.pack()
    #plot
    fig = cb.MPLGraph(ssc_feat[:,2], ssc_feat[:,3])
    print(pane_win.panes())
    p=pane_win.panes()
    graph_page = cb.GraphPage('<window object: \'.!panedwindow.!label\'>', fig)
    graph_page.add_mpl_figure()
    
    window1.mainloop()

    




