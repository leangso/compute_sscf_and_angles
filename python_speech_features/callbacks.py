#Edited by Thomas Debeuret (not author) : update of module
#callbacks
from tkinter import *
import tkinter as tk
import matplotlib.pyplot as plt
import matplotlib
matplotlib.use("TkAgg")
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg 
#Thomas Edit : NavigationToolbar2TkAgg is not used anymore, it has been rename NavigationToolbar2Tk in the module
from matplotlib.backends.backend_tkagg import NavigationToolbar2Tk as NavigationToolbar2TkAgg
from matplotlib.figure import Figure
import numpy as np
from getclass import getclass

def open():
    tkFileDialog.askopenfilename()
    
def save():
    tkMessageBox.showinfo("Enregistrer")
    
def saveas():
    tkMessageBox.showinfo("save as...")

    
def trace_SSCF1_SSCF2(ssc_feat) :
    print('COucou')
##  plt.plot(ssc_feat[:, 1], ssc_feat[:, 2], '+', linewith=1)
##  plt.xlabel('SSCF1')
##  plt.ylabel('SCCF2')
##  plt.title('idu2 - SSCF1-SSCF2')

def trace_SSCF2_SSCF3(ssc_feat) :
    print('Qui pisse au vent n\'amasse pas mousse')

def trace_SSCF3_SSCF4(ssc_feat) :
    print('Pierre qui roule vole un boeuf')

def trace_SSCF4_SSCF5(ssc_feat):
    print('Chat échaudé se rince les dents')

def test():
    print('Waterlooooooooo')

class GraphPage(tk.Frame):

    def __init__(self, parent, fig):
        tk.Frame.__init__(self, parent)
        self.fig=fig
        self.title_label = tk.Label(self, text="SSCF")
        self.title_label.pack()
        self.pack()

    def add_mpl_figure(self):
        self.mpl_canvas = FigureCanvasTkAgg(self.fig, self)
        self.mpl_canvas.show()
        self.mpl_canvas.get_tk_widget().pack(side=tk.TOP, fill=tk.BOTH, expand=False)

        self.toolbar = NavigationToolbar2TkAgg(self.mpl_canvas, self)
        self.toolbar.update()
        self.mpl_canvas._tkcanvas.pack(side=tk.TOP, fill=tk.BOTH, expand=False)
        
    def set_fig (self,fig) :
        self.fig=fig

##    def replace_mpl_figure(self):
        
                


class MPLGraph(Figure):

    def __init__(self,xdata, ydata):
        Figure.__init__(self, figsize=(5,5), dpi=100)
        self.xdata=xdata
        self.ydata=ydata
        self.plot = self.add_subplot(111)
        self.plot.plot(self.xdata, self.ydata, marker='+')
        self.plot.set_xlim(np.min(self.xdata)-100, np.max(self.xdata)+100)
        self.plot.set_ylim(np.min(self.ydata)-100, np.max(self.ydata)+100)
        print(getclass(self.plot))

##    def update(self) :
##        self.plot.plot(self.xdata, self.ydata, marker='+')
##        self.plot.update_xlim(np.min(self.xdata)-100, np.max(self.xdata)+100)
##        self.plot.set_ylim(np.min(self.ydata)-100, np.max(self.ydata)+100)        

##    def update_xdata(self, value, ssc_feat, graph_page, win) :
##        self.update()
##        graph_page.mpl_canvas.get_tk_widget().destroy()
##        self.xdata=ssc_feat[:, value]
##        fig = MPLGraph(ssc_feat[:,value], self.ydata)
##        self.xdata=fig.xdata
##        self.ydata=fig.ydata
##        graph_page = GraphPage(win, self)
##        graph_page.add_mpl_figure()
##        self=init(ssc_feat[:,value], self.ydata)
##        


    def update_xdata(self, value, ssc_feat, graph_page, win) :
        graph_page.set_fig(MPLGraph(ssc_feat[:, value], self.ydata))
        #graph_page.replace_mpl_figure()
        graph_page.add_mpl_figure()

def create_graph_sscf(ssc_feat, win) :
    
    #win_graph_sscf = tk.Tk()
    fig = MPLGraph(ssc_feat[:,2], ssc_feat[:,3])
    graph_page = GraphPage(win, fig)
    graph_page.add_mpl_figure()

    value_x = tk.StringVar()
    bouton_x1 = Radiobutton(win, text="SSCF1", variable=value_x, value=1, command = lambda : fig.update_xdata(1, ssc_feat, graph_page, win))
    bouton_x2 = Radiobutton(win, text="SSCF2", variable=value_x, value=2, command = lambda : fig.update_xdata(2, ssc_feat, graph_page, win))
    bouton_x3 = Radiobutton(win, text="SSCF3", variable=value_x, value=3, command = lambda : fig.update_xdata(3, ssc_feat, graph_page, win))
    bouton_x4 = Radiobutton(win, text="SSCF4", variable=value_x, value=4, command = lambda : fig.update_xdata(4, ssc_feat, graph_page, win))
    bouton_x5 = Radiobutton(win, text="SSCF5", variable=value_x, value=5, command = lambda : fig.update_xdata(5, ssc_feat, graph_page, win))
    bouton_x1.pack(side='left')
    bouton_x2.pack(side='left')
    bouton_x3.pack(side='left')
    bouton_x4.pack(side='left')
    bouton_x5.pack(side='left')

    value_y = tk.StringVar()
    bouton_y1 = Radiobutton(win, text="SSCF1", variable=value_y, value=1)
    bouton_y2 = Radiobutton(win, text="SSCF2", variable=value_y, value=2)
    bouton_y3 = Radiobutton(win, text="SSCF3", variable=value_y, value=3)
    bouton_y4 = Radiobutton(win, text="SSCF4", variable=value_y, value=4)
    bouton_y5 = Radiobutton(win, text="SSCF5", variable=value_y, value=5)
    bouton_y1.pack(side='left')
    bouton_y2.pack(side='left')
    bouton_y3.pack(side='left')
    bouton_y4.pack(side='left')
    bouton_y5.pack(side='left')
##    for i in range(3):
##        b = RadioButton(win, variable=value, text=etiqs[i], value=vals[i])
##        b.pack(side='left', expand=1)




    
